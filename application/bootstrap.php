<?php

// Connect core files
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';

//Form parse data model

require_once 'core/route.php';

Route::start(); // start routing mechanism