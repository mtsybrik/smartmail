<?php

/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 9/13/16
 * Time: 7:01 PM
 */
class controller_mail extends Controller
{
    public function action_send(){
       $message_body = model_mail::receive_data();
       $response = model_mail::send_data($message_body);

        if(isset($response)){
           //success
           $result['status'] = $response;
           echo json_encode($result);
       }
        else{
            $result['status'] = 'error';
            echo json_encode($result);
        }

    }
}