<?php

/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 9/13/16
 * Time: 10:30 PM
 */
class model_mail extends Model
{
    public static function receive_data()
    {
        //extract data from the post
        $message_body = file_get_contents('php://input');
        return $message_body;
    }
    public static function send_data($message_body)
    {
        //debug coockie for testing
//            $debug_cookie = 'XDEBUG_SESSION=PHPSTORM;path=/;domain=smartmail_back';
        $options = array(
            CURLOPT_URL => 'http://smartmail_back/message/send',
            CURLOPT_POST => count($message_body),
            CURLOPT_POSTFIELDS => $message_body,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER         => false  // don't return headers); // return result
        );

        //open connection
        $ch = curl_init();

        //curl options
        curl_setopt_array($ch, $options);
        //execute post
        $result = curl_exec($ch);

        //close connection
        curl_close($ch);

        return $result;
    }
}