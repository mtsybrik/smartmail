<!-- HTML form for validation  -->
<form action="" method="post" id="register-form" novalidate="novalidate">

    <div id="form-content">
        <fieldset>

            <div class="fieldgroup">
                <input type="text" name="name" placeholder="ФИО" id="name">
            </div>

            <div class="fieldgroup">
                <input type="text" name="email" placeholder="Email-адрес" id="email">
            </div>

            <div class="fieldgroup">
                <textarea name="message" placeholder="Сообщение" rows="3" id="message"></textarea>
            </div>

            <div class="fieldgroup">
                <button type="submit" class="submit">Отправить</button>
                <button type="reset" class="submit">Очистить</button>
            </div>

        </fieldset>
    </div>
<div id="popup_bkg"></div>
    <div id="popup_box">
        <span id="close">&times;</span>
        <div class="g-recaptcha" data-sitekey="6LddjhcTAAAAABHc7vkHRNn6iqR9df-iC3q4-f3t" data-callback="verifyCallback"></div>
    </div>
</form>
<!-- END HTML form for validation -->