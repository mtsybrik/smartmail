
    (function($,W,D)
    {
        var JQUERY4U = {};
        JQUERY4U.UTIL =
        {
            setupFormValidation: function()
            {
                //form validation rules
                $("#register-form").validate({
                    onfocusout: false,
                    onkeyup: false,
                    onclick: false,
                    rules: {
                        name: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        message:"required"

                    },
                    messages: {
                        name: "Пожалуйста введите ваше ФИО",
                        email: "Пожалуйста введите корректный email адресс",
                        message:"Пожалуйста заполните поле \"Cообщение\""
                    },
                    highlight: function(element, errorClass, validClass) {
                        $(element).addClass("wrong").removeClass(validClass);
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).removeClass("wrong").addClass(validClass);

                    },
                    submitHandler: function(form) {
                        $('#popup_bkg').fadeIn();
                        $('#popup_box').fadeIn();
                    }
                });
            }
        }
        //when the dom has loaded setup form validation rules
        $(D).ready(function($) {
            JQUERY4U.UTIL.setupFormValidation();
        });
    })(jQuery, window, document);
 $(document).ready(function(){
     $('#close').click(function() {
         $('#popup_bkg').fadeOut();
         $('#popup_box').fadeOut();
     })
     $('#popup_bkg').click(function() {
         $('#popup_bkg').fadeOut();
         $('#popup_box').fadeOut();
     })
 });
 var verifyCallback = function(response) {
     var obj = new Object();
     obj.name = $('#name').val();
     obj.email  = $('#email').val();
     obj.message = $('#message').val();
     var jsonString= JSON.stringify(obj);
     $.ajax({
         type: "POST",
         url: "/mail/send",
         data: jsonString,
         contentType: "application/json; charset=utf-8",
         contentEncoding: "gzip",
         dataType: "json",
         async:false,
         success: function(data){
                     alert(data.status);
                 },
         failure: function(errMsg) {
             alert(errMsg);
         }
     });
     $('#popup_bkg').fadeOut();
     $('#popup_box').fadeOut();
     location.reload()
 };